package com.cogni.congnitest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CongniTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(CongniTestApplication.class, args);
	}

}
