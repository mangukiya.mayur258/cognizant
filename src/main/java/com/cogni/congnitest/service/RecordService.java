package com.cogni.congnitest.service;

import com.cogni.congnitest.model.Record;
import com.cogni.congnitest.utils.CsvUtils;
import com.cogni.congnitest.utils.XmlUtils;
import java.util.List;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class RecordService {
	
	public List<Record> validate(MultipartFile file) throws Exception {
		if (!file.getOriginalFilename().endsWith(".csv") && !file.getOriginalFilename().endsWith(".xml")) {
			throw new RuntimeException("File is in invalid format.");
		}
		if (file.getOriginalFilename().endsWith(".csv")) {
			return CsvUtils.createRecordsFromFile(file);
		}
		return XmlUtils.createRecordsFromFile(file);
	}
}
