package com.cogni.congnitest.utils;

import com.cogni.congnitest.model.Record;
import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class FieldValidationUtil {
	
	private static final Logger logger = LoggerFactory.getLogger(FieldValidationUtil.class);
	
	public static void validateEndBalanceRecords(Record record) {
		logger.debug("Mutation Diff : ", new BigDecimal(record.getStartBalance() + record.getMutation())
				.setScale(2, BigDecimal.ROUND_HALF_UP));
		logger.debug("End Balance : ", new BigDecimal(record.getEndBalance())
				.setScale(2, BigDecimal.ROUND_HALF_UP));
		if (!new BigDecimal(record.getStartBalance() + record.getMutation())
				.setScale(2, BigDecimal.ROUND_HALF_UP)
				.equals(new BigDecimal(record.getEndBalance()).setScale(2, BigDecimal.ROUND_HALF_UP))) {
			record.setErrorMessage("End Balance is not valid");
		}
	}
	
	public static void validateReferenceRecords(Record record, List<Record> records) {
		logger.debug("Record Rer IF : ", record.getReferenceId());
		if (records.stream()
				.filter(o1 -> record.getReferenceId().equals(o1.getReferenceId()))
				.collect(Collectors.toList()).size() > 0) {
			record.setErrorMessage((record.getErrorMessage() != null ? record.getErrorMessage()
					+ "\n" : "") + "Reference id is already used");
		}
	}
	
}
