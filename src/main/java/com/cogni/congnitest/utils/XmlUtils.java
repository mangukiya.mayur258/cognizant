package com.cogni.congnitest.utils;

import com.cogni.congnitest.model.Record;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XmlUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(XmlUtils.class);
	
	public static List<Record> createRecordsFromFile(MultipartFile file) throws Exception {
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = dBuilder.parse(file.getInputStream());
		doc.getDocumentElement().normalize();
		
		logger.debug("Root element :" + doc.getDocumentElement().getNodeName());
		
		NodeList nList = doc.getElementsByTagName("record");
		
		logger.debug("----------------------------");
		List<Record> records = new ArrayList<>();
		try {
			for (int temp = 0; temp < nList.getLength(); temp++) {
				
				Node nNode = nList.item(temp);
				
				logger.debug("\nCurrent Element :" + nNode.getNodeName());
				
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					
					Element eElement = (Element) nNode;
					Record record = new Record(Long.parseLong(eElement.getAttribute("reference")),
							eElement.getElementsByTagName("accountNumber").item(0).getTextContent(),
							eElement.getElementsByTagName("description").item(0).getTextContent(),
							Double.parseDouble(eElement.getElementsByTagName("startBalance").item(0).getTextContent()),
							Double.parseDouble(eElement.getElementsByTagName("endBalance").item(0).getTextContent()),
							Double.parseDouble(eElement.getElementsByTagName("mutation").item(0).getTextContent())
					);
					FieldValidationUtil.validateEndBalanceRecords(record);
					FieldValidationUtil.validateReferenceRecords(record, records);
					records.add(record);
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		}
		return records;
	}
}
