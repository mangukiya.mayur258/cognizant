package com.cogni.congnitest.utils;

import com.cogni.congnitest.model.Record;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.multipart.MultipartFile;

public class CsvUtils {
	
	private static final Logger logger = LoggerFactory.getLogger(CsvUtils.class);
	
	private static final String CVS_SPLIT_BY = ",";
	
	public static List<Record> createRecordsFromFile(MultipartFile file) {
		
		BufferedReader br = null;
		
		List<Record> records = new ArrayList<>();
		try {
			String line = "";
			br = new BufferedReader(new InputStreamReader(file.getInputStream()));
			int counter = 0;
			while ((line = br.readLine()) != null) {
				if (counter++ == 0) {
					continue;
				}
				logger.debug(line);
				String[] recordLine = line.split(CVS_SPLIT_BY);
				Record record = new Record(Long.parseLong(recordLine[0]), recordLine[1], recordLine[2],
						Double.parseDouble(recordLine[3]), Double.parseDouble(recordLine[5]),
						Double.parseDouble(recordLine[4]));
				FieldValidationUtil.validateEndBalanceRecords(record);
				FieldValidationUtil.validateReferenceRecords(record, records);
				records.add(record);
			}
		} catch (Exception e) {
			logger.error(e.getMessage(), e);
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (Exception e) {
					logger.error(e.getMessage(), e);
				}
			}
		}
		return records;
	}
}
