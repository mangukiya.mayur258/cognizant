package com.cogni.congnitest.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Record {
	private Long referenceId;
	private String accountNumber;
	private String description;
	private Double startBalance;
	private Double endBalance;
	private Double mutation;
	private String errorMessage;
	
	public Record(Long referenceId, String accountNumber, String description, Double startBalance, Double endBalance, Double mutation) {
		this.referenceId = referenceId;
		this.accountNumber = accountNumber;
		this.description = description;
		this.startBalance = startBalance;
		this.endBalance = endBalance;
		this.mutation = mutation;
	}
}
