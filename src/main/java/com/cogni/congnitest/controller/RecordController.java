package com.cogni.congnitest.controller;

import com.cogni.congnitest.model.Record;
import com.cogni.congnitest.service.RecordService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class RecordController {
	
	@Autowired
	private RecordService recordService;
	
	@PostMapping("/records/validate")
	public ResponseEntity<List<Record>> uploadFile(@RequestParam("file") MultipartFile file) {
		try {
			List<Record> records = recordService.validate(file);
			return ResponseEntity.status(HttpStatus.OK).body(records);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(null);
		}
	}
}
