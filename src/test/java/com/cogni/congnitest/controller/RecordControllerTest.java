package com.cogni.congnitest.controller;

import com.cogni.congnitest.model.Record;
import com.cogni.congnitest.service.RecordService;
import java.util.Collections;
import java.util.List;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;

@RunWith(MockitoJUnitRunner.class)
public class RecordControllerTest {
	
	private static final String ACCOUNT_NUMBER = "NL91RABO0315273637";
	private static final Long REF_NO = 187997L;
	private static final String DESCRIPTION = "Clothes for Rik King";
	private static final Double START_BALANCE = 57.6D;
	private static final Double MUTATION = -32.98D;
	private static final Double END_BALANCE = 24.62D;
	
	@InjectMocks
	private RecordController recordController;
	
	@Mock
	private RecordService recordService;
	
	@Mock
	private MultipartFile multipartFile;
	
	@Before
	public void setUp() throws Exception {
		Record record = new Record(REF_NO, ACCOUNT_NUMBER, DESCRIPTION, START_BALANCE, END_BALANCE, MUTATION);
		Mockito.when(recordService.validate(Mockito.any(MultipartFile.class)))
				.thenReturn(Collections.singletonList(record));
	}
	
	@Test
	public void testUploadFile() {
		ResponseEntity<List<Record>> recordList = recordController.uploadFile(multipartFile);
		// Assert
		Assert.assertThat(recordList.getStatusCode(), CoreMatchers.equalTo(HttpStatus.OK));
		Assert.assertThat(recordList.getBody().size(), CoreMatchers.equalTo(1));
		Record recordLocal = recordList.getBody().get(0);
        Assert.assertThat(recordLocal.getReferenceId(), CoreMatchers.equalTo(REF_NO));
        Assert.assertThat(recordLocal.getAccountNumber(), CoreMatchers.equalTo(ACCOUNT_NUMBER));
        Assert.assertThat(recordLocal.getDescription(), CoreMatchers.equalTo(DESCRIPTION));
        Assert.assertThat(recordLocal.getStartBalance(), CoreMatchers.equalTo(START_BALANCE));
        Assert.assertThat(recordLocal.getEndBalance(), CoreMatchers.equalTo(END_BALANCE));
        Assert.assertThat(recordLocal.getMutation(), CoreMatchers.equalTo(MUTATION));
	}
}
