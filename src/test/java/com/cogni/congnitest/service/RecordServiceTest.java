package com.cogni.congnitest.service;

import com.cogni.congnitest.model.Record;
import java.io.InputStream;
import java.util.List;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.web.multipart.MultipartFile;

@RunWith(MockitoJUnitRunner.class)
public class RecordServiceTest {
	
	private static final Long REF_NO = 187997L;
	private static final String DESCRIPTION = "Clothes for Rik King";
	private static final Double START_BALANCE = 57.6D;
	private static final Double MUTATION = -32.98D;
	private static final Double END_BALANCE = 24.62D;
	private static final String ACCOUNT_NUMBER = "NL91RABO0315273637";
	
	@InjectMocks
	private RecordService recordService;
	
	@Mock
	private MultipartFile multipartFile;
	
	@Test
	public void testValidate() throws Exception {
		mockStream(".csv");
		List<Record> recordList = recordService.validate(multipartFile);
		// Assert
		Assert.assertThat(recordList.size(), CoreMatchers.equalTo(10));
		Record recordLocal = recordList.get(0);
        Assert.assertThat(recordLocal.getReferenceId(), CoreMatchers.equalTo(177666L));
        Assert.assertThat(recordLocal.getAccountNumber(), CoreMatchers.equalTo("NL93ABNA0585619023"));
        Assert.assertThat(recordLocal.getDescription(), CoreMatchers.containsString("Flowers for Rik Theu"));
        Assert.assertThat(recordLocal.getStartBalance(), CoreMatchers.equalTo(44.85D));
        Assert.assertThat(recordLocal.getEndBalance(), CoreMatchers.equalTo(22.61D));
        Assert.assertThat(recordLocal.getMutation(), CoreMatchers.equalTo(-22.24D));
	}
	
	@Test(expected = RuntimeException.class)
	public void testValidateThenExceptionWillBeThrown() throws Exception {
		Mockito.when(multipartFile.getOriginalFilename()).thenReturn(".txt");
		recordService.validate(multipartFile);
	}
	
	@Test
	public void testValidateWhenFileIsXml() throws Exception {
		mockStream(".xml");
		List<Record> recordList = recordService.validate(multipartFile);
		// Assert
		Assert.assertThat(recordList.size(), CoreMatchers.equalTo(10));
		Record recordLocal = recordList.get(0);
        Assert.assertThat(recordLocal.getReferenceId(), CoreMatchers.equalTo(REF_NO));
        Assert.assertThat(recordLocal.getAccountNumber(), CoreMatchers.equalTo(ACCOUNT_NUMBER));
        Assert.assertThat(recordLocal.getDescription(), CoreMatchers.equalTo(DESCRIPTION));
        Assert.assertThat(recordLocal.getStartBalance(), CoreMatchers.equalTo(START_BALANCE));
        Assert.assertThat(recordLocal.getEndBalance(), CoreMatchers.equalTo(END_BALANCE));
        Assert.assertThat(recordLocal.getMutation(), CoreMatchers.equalTo(MUTATION));
	}
	
	private void mockStream(String fileType) throws Exception {
		ClassLoader classLoader = new RecordServiceTest().getClass().getClassLoader();
		InputStream inputStream = classLoader.getResourceAsStream("records" + fileType);
		Mockito.when(multipartFile.getInputStream()).thenReturn(inputStream);
		Mockito.when(multipartFile.getOriginalFilename()).thenReturn(fileType);
	}
}
