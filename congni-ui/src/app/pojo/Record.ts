export class Record {
    referenceId: number;
    accountNumber: string;
    description: string;
    startBalance: number;
    endBalance: number;
    mutation: number;
    errorMessage: string;
}