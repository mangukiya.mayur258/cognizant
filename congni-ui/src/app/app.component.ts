import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Upload File';
  description = 'Upload Records with CSV or XML formats only to validate either data you are providing are right or not.';

  constructor() { }
}
