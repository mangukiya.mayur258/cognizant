import { Component, OnInit } from '@angular/core';
import { UploadFileService } from '../upload-file.service';
import { HttpResponse, HttpEventType } from '@angular/common/http';
import { Record } from 'src/app/pojo/Record';
import { DataSource } from '@angular/cdk/table';
import { BehaviorSubject, Observable } from 'rxjs';
import {MatTableDataSource} from '@angular/material';

@Component({
  selector: 'form-upload',
  templateUrl: './form-upload.component.html',
  styleUrls: ['./form-upload.component.css']
})
export class FormUploadComponent implements OnInit {

  selectedFiles: FileList;
  currentFileUpload: File;
  progress: { percentage: number } = { percentage: 0 };
  records: Record[] = [];
  displayedColumns: string[] = ['referenceId', 'accountNumber', 'description', 'startBalance', 'mutation', 'endBalance', 'errorMessage'];
  dataSource = new MatTableDataSource([]);

  constructor(private uploadService: UploadFileService) { }

  ngOnInit() {
  }

  selectFile(event) {
    this.selectedFiles = event.target.files;
  }

  upload() {
    this.progress.percentage = 0;

    this.currentFileUpload = this.selectedFiles.item(0);
    this.uploadService.pushFileToStorage(this.currentFileUpload).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.progress.percentage = Math.round(100 * event.loaded / event.total);
      } else if (event instanceof HttpResponse) {
        this.records = JSON.parse(String(event.body));
        this.dataSource = new MatTableDataSource(this.records);
      }
    });

    this.selectedFiles = undefined;
  }

}

export class ExampleDataSource extends DataSource<Record> {
  data = new BehaviorSubject<Record[]>([]);

  connect(): Observable<Record[]> {
    return this.data;
  }

  disconnect() {}
}
